\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {1}Orthogonalisierungsverfahren}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Eigenschaften orthogonaler Matrizen}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Anwendung: Lineare Ausgleichsgeraden}{1}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\noindent \unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ \nobreakspace {}\unhbox \voidb@x \setbox \@tempboxa \hbox {\color@begingroup \kern \fboxsep {\leavevmode {\color {red}End of Lecture 1}}\kern \fboxsep \endgraf \endgroup }\XC@frameb@x \relax \nobreakspace {}\unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ }{2}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Gram-Schmidt-Verfahren}{4}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Householder-Transformationen}{5}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\noindent \unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ \nobreakspace {}\unhbox \voidb@x \setbox \@tempboxa \hbox {\color@begingroup \kern \fboxsep {\leavevmode {\color {red}End of Lecture 2}}\kern \fboxsep \endgraf \endgroup }\XC@frameb@x \relax \nobreakspace {}\unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ }{6}{section*.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\noindent \unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ \nobreakspace {}\unhbox \voidb@x \setbox \@tempboxa \hbox {\color@begingroup \kern \fboxsep {\leavevmode {\color {red}End of Lecture 3}}\kern \fboxsep \endgraf \endgroup }\XC@frameb@x \relax \nobreakspace {}\unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ }{8}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Singul\IeC {\"a}rwertzerlegung}{8}{section.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.5.1}Anwendung bei linearen Ausgleichsproblemen}{8}{subsection.1.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\noindent \unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ \nobreakspace {}\unhbox \voidb@x \setbox \@tempboxa \hbox {\color@begingroup \kern \fboxsep {\leavevmode {\color {red}End of Lecture 4}}\kern \fboxsep \endgraf \endgroup }\XC@frameb@x \relax \nobreakspace {}\unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ }{8}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {2}Gradienten- und CG-Verfahren}{9}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\noindent \unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ \nobreakspace {}\unhbox \voidb@x \setbox \@tempboxa \hbox {\color@begingroup \kern \fboxsep {\leavevmode {\color {red}End of Lecture 5}}\kern \fboxsep \endgraf \endgroup }\XC@frameb@x \relax \nobreakspace {}\unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ }{9}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Abstiegsverfahren - Basics}{9}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Konstruktion von Abstiegsverfahren}{10}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}effiziente Schrittweiten}{10}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Gradientenbezogene Suchrichtungen}{11}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Schrittweitenbestimmung}{12}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}exakte Schrittweiten}{12}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\noindent \unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ \nobreakspace {}\unhbox \voidb@x \setbox \@tempboxa \hbox {\color@begingroup \kern \fboxsep {\leavevmode {\color {red}End of Lecture 6}}\kern \fboxsep \endgraf \endgroup }\XC@frameb@x \relax \nobreakspace {}\unhbox \voidb@x \leaders \hrule height .7ex width 1ex depth -0.6ex\hfill \kern \z@ }{12}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Armijo-Schrittregel}{13}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {appendix}{\chapternumberline {A}Insights from the exercise sheets}{15}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.0}Sheet 0}{15}{section.A.0}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Sheet 1}{15}{section.A.1}
